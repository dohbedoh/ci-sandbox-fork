package com.dohbedoh.service;

import com.dohbedoh.exception.FireChuckNorrisException;
import com.dohbedoh.model.Company;
import com.dohbedoh.model.Employee;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

/**
 * Test of the {@link CompanyService}.
 *
 * Created by Allan BURDAJEWICZ on 1/10/2015.
 */
public class CompanyServiceTest {

    Company company;
    CompanyService companyService = new CompanyService();

    @Before
    public void initializeCompany() {
        Collection employees = new ArrayList<Employee>();
        employees.add(new Employee("John", "Rambo", 30, 30000, new Date()));
        employees.add(new Employee("Chuck", "Norris", -1, Integer.MAX_VALUE, new Date()));
        employees.add(new Employee("Matt", "Stone", 45, 654321, new Date()));
        employees.add(new Employee("John", "Doe", 26, 60000, new Date()));
        company = new Company("Dohbedoh Corporation", employees);
    }

    @Test
    public void testHireOneEmployee() {
        int companySize = company.getEmployees().size();
        Employee newEmployee = new Employee("Hugh", "Jackman", 45, 1000000, new Date());
        companyService.hire(company, newEmployee);

        Assert.assertEquals(companySize+1, company.getEmployees().size());
        Assert.assertTrue(company.getEmployees().contains(newEmployee));
    }

    @Test
    public void testHireManyEmployee() {
        int companySize = company.getEmployees().size();
        Employee newEmployee1 = new Employee("Hugh", "Jackman", 45, 1000000, new Date());
        Employee newEmployee2 = new Employee("Trey", "Parker", 40, 123456, new Date());
        companyService.hire(company, newEmployee1, newEmployee2);

        Assert.assertEquals(companySize+2, company.getEmployees().size());
        Assert.assertTrue(company.getEmployees().contains(newEmployee1));
        Assert.assertTrue(company.getEmployees().contains(newEmployee2));
    }

    @Test
    public void testHireAlreadyRegisteredEmployee() {
        int companySize = company.getEmployees().size();
        Employee newEmployee = new Employee("John", "Doe", 0, 0, new Date());
        companyService.hire(company, newEmployee);

        Assert.assertEquals(companySize, company.getEmployees().size());
        Assert.assertTrue(company.getEmployees().contains(newEmployee));
    }

    @Test
    public void testFireOneEmployee() throws FireChuckNorrisException {
        int companySize = company.getEmployees().size();
        Employee employee1 = new Employee("John", "Doe", 0, 0, new Date());
        companyService.fire(company, employee1);

        Assert.assertEquals(companySize-1, company.getEmployees().size());
        Assert.assertFalse(company.getEmployees().contains(employee1));
    }

    @Test
    public void testFireManyEmployee() throws FireChuckNorrisException {
        int companySize = company.getEmployees().size();
        Employee employee1 = new Employee("Matt", "Stone", 0, 0, new Date());
        Employee employee2 = new Employee("John", "Doe", 0, 0, new Date());
        companyService.fire(company, employee1, employee2);

        Assert.assertEquals(companySize-2, company.getEmployees().size());
        Assert.assertFalse(company.getEmployees().contains(employee1));
        Assert.assertFalse(company.getEmployees().contains(employee2));
    }

    @Test(expected = FireChuckNorrisException.class)
    public void testFireChuckNorris() throws FireChuckNorrisException {
        Employee oldEmployee = new Employee("Chuck", "Norris", 0, 0, new Date());
        companyService.fire(company, oldEmployee);
    }

    @Test
    public void testFireNotRegisteredEmployee() throws FireChuckNorrisException {
        int companySize = company.getEmployees().size();
        Employee employee1 = new Employee("John", "Doe", 0, 0, new Date());
        Employee employee2 = new Employee("Hugh", "Jackman", 0, 0, new Date());
        companyService.fire(company, employee1, employee2);

        Assert.assertEquals(companySize-1, company.getEmployees().size());
        Assert.assertFalse(company.getEmployees().contains(employee1));
        Assert.assertFalse(company.getEmployees().contains(employee2));
    }
}
