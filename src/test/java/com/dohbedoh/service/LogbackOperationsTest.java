package com.dohbedoh.service;

import org.junit.Test;

/**
 * Created by Allan BURDAJEWICZ on 1/10/2015.
 */
public class LogbackOperationsTest {

    /**
     * Test the log levels by modifying the logback-test.xml root logger and service logger.
     */
    @Test
    public void testMessageLevels() {
        LogbackOperations ops = new LogbackOperations();
        ops.trace("This is a TRACE message");
        ops.debug("This is a DEBUG message");
        ops.info("This is a INFO message");
        ops.warn("This is a WARN message");
        ops.error("This is a ERROR message");
    }
}
