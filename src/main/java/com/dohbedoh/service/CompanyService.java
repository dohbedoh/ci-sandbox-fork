package com.dohbedoh.service;

import com.dohbedoh.exception.FireChuckNorrisException;
import com.dohbedoh.model.Company;
import com.dohbedoh.model.Employee;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Simple Service: provide operation for a {@link Company}.
 * Created by Allan on 1/10/2015.
 */
public class CompanyService {

    private final Logger log = LoggerFactory.getLogger(CompanyService.class);

    /**
     * Fire employees.
     *
     * @param company   The {@link Company}
     * @param employees The set of {@link Employee}
     * @throws FireChuckNorrisException If you try to fire Chuck Norris
     */
    public void fire(Company company, Employee... employees) throws FireChuckNorrisException {
        for (Employee employee : employees) {
            if ("Norris".equals(employee.getLastName()) && "Chuck".equals(employee.getFirstName())) {
                throw new FireChuckNorrisException();
            }

            if (company.getEmployees().contains(employee)) {
                company.getEmployees().remove(employee);
            } else {
                log.warn("Employee {} is not registered in this company.");
            }
        }
    }

    /**
     * Hire employees.
     *
     * @param company   The {@link Company}
     * @param employees The set of {@link Employee}
     */
    public void hire(Company company, Employee... employees) {
        for (Employee employee : employees) {
            if (!company.getEmployees().contains(employee)) {
                company.getEmployees().add(employee);
            } else {
                log.warn("Employee {} is already registered in this company.");
            }
        }
    }
}
