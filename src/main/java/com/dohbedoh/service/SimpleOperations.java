package com.dohbedoh.service;

/**
 * Service class: Simple operations for testing JUnit.
 *
 * Created by Allan BURDAJEWICZ on 29/09/2015.
 */
public class SimpleOperations {

    private int value;

    public SimpleOperations() {
        this.value = 0;
    }

    public SimpleOperations(int value) {
        this.value = value;
    }

    /**
     * Perform simple addition.
     *
     * @param a The value to add
     * @return current value + a as an int
     */
    public SimpleOperations plus(int a) {
        return new SimpleOperations(value+a);
    }

    /**
     * Perform simple subtraction and return the value.
     *
     * @param a The value to subtract
     * @return current value - a as an int
     */
    public SimpleOperations minus(int a) {
        return new SimpleOperations(value-a);
    }

    /**
     * Perform simple subtraction and return the value.
     *
     * @param a The value to subtract
     * @return current value - a as an int
     */
    public SimpleOperations divide(int a) {
        return new SimpleOperations(value/a);
    }

    /**
     * Return the current value.
     * @return The value
     */
    public int getValue() {
        return value;
    }
}
