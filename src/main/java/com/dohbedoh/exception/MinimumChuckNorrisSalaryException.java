package com.dohbedoh.exception;

/**
 * Created by Allan on 1/10/2015.
 */
public class MinimumChuckNorrisSalaryException extends Exception {

    public MinimumChuckNorrisSalaryException() {
        super("You should reconsider before reducing Chuck Norris salary, your life depends on it...");
    }

    public MinimumChuckNorrisSalaryException(Throwable cause) {
        super("You should reconsider before reducing Chuck Norris salary, your life depends on it...", cause);
    }
}
