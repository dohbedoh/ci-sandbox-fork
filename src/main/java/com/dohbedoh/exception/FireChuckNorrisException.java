package com.dohbedoh.exception;

/**
 * Exception raised whenever you try to fire Chuck Norris.
 *
 * Created by Allan on 1/10/2015.
 */
public class FireChuckNorrisException extends Exception {

    public FireChuckNorrisException() {
        super("You cannot fire Chuck Norris! Who do you think you are...");
    }

    public FireChuckNorrisException(Throwable cause) {
        super("You cannot fire Chuck Norris! Who do you think you are...", cause);
    }
}
