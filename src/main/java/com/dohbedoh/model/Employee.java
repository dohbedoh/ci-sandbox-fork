package com.dohbedoh.model;

import java.util.Date;

/**
 * Simple Employee: first name, last name, age, entry date.
 *
 * Created by Allan on 1/10/2015.
 */
public class Employee {

    private String firstName;
    private String lastName;
    private int age;
    private int salary;
    private Date entryDate;

    public Employee() {
    }

    public Employee(
            String firstName,
            String lastName,
            int age,
            int salary,
            Date entryDate) {

        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.salary = salary;
        this.entryDate = entryDate == null ? null : new Date(entryDate.getTime());
    }

    public Employee(Employee employee) {

        this.firstName = employee.getFirstName();
        this.lastName = employee.getLastName();
        this.age = employee.getAge();
        this.salary = employee.getSalary();
        this.entryDate = employee.getEntryDate();
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getAge() {
        return age;
    }

    public int getSalary() {
        return salary;
    }

    public Date getEntryDate() {
        //Date is Mutable
        return entryDate == null ? null : new Date(entryDate.getTime());
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        Employee employee = (Employee) obj;
        return (firstName == employee.firstName || (firstName != null && firstName.equals(employee.getFirstName())))
                && (lastName == employee.lastName || (lastName != null && lastName.equals(employee.getLastName())));
    }

    @Override
    public int hashCode() {
        int result = getFirstName() != null ? getFirstName().hashCode() : 0;
        result = 31 * result + (getLastName() != null ? getLastName().hashCode() : 0);
        return result;
    }
}
